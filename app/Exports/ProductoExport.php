<?php

namespace App\Exports;

use App\empleado;
use App\producto;
use App\punto;
use App\tipoproducto;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ProductoExport implements FromView
{

    public function view(): View{
        $empleado = empleado::all();
        $producto = producto::all();
        $punto = punto::all();
        $tipos = tipoproducto::all();
        return view('Exportar.Producto', compact('empleado','producto','punto','tipos'));
    }

}
