<?php

namespace App\Exports;

use App\empleado;
use App\estado;
use App\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class EmpleadoExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function View(): View{
        $Empleado = empleado::all();
        $user = User::all();
        $estado = estado::all();
        return view('Exportar.Empleado', compact('Empleado','user','estado'));
    }

}
