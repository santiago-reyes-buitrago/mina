<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class empleado extends Model
{
    //
    use HasRoles;
    protected $table = 'Empleados';
}
