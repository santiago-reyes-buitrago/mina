<?php

namespace App\Http\Controllers;

use App\tipoproducto;
use Illuminate\Http\Request;

class TipoproductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tipoproducto  $tipoproducto
     * @return \Illuminate\Http\Response
     */
    public function show(tipoproducto $tipoproducto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tipoproducto  $tipoproducto
     * @return \Illuminate\Http\Response
     */
    public function edit(tipoproducto $tipoproducto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tipoproducto  $tipoproducto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipoproducto $tipoproducto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tipoproducto  $tipoproducto
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipoproducto $tipoproducto)
    {
        //
    }
}
