<?php

namespace App\Http\Controllers;

use App\punto;
use Illuminate\Http\Request;

class PuntoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\punto  $punto
     * @return \Illuminate\Http\Response
     */
    public function show(punto $punto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\punto  $punto
     * @return \Illuminate\Http\Response
     */
    public function edit(punto $punto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\punto  $punto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, punto $punto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\punto  $punto
     * @return \Illuminate\Http\Response
     */
    public function destroy(punto $punto)
    {
        //
    }
}
