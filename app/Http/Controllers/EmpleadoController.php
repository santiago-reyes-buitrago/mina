<?php

namespace App\Http\Controllers;

use App\empleado;
use App\estado;
use App\Exports\EmpleadoExport;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;


class EmpleadoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $Empleado = empleado::all();
        $user = User::all();
        $estado = estado::all();
        $Activos = DB::table('Empleados')->select(DB::raw('count(*) as Activo'))->where('estado','=','1')->get();
        $Inactivos = DB::table('Empleados')->select(DB::raw('count(*) as Inactivo'))->where('estado','=','2')->get();
        return view('Empleados.Principal',compact('Empleado','user','estado','Activos','Inactivos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $user = User::all();
        return view('Empleados.Crear',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $request->validate([
            'nombre' => 'required',
            'apellido' => 'required',
            'user_id' => 'unique:Empleados|required',
            'Documento' => 'unique:Empleados|numeric|required',
            'telefono' => 'unique:Empleados|numeric',
            'direccion' => 'required',
            'estado' => 'required',
        ]);

        $empleado= $request->except('_token');

        empleado::insert($empleado);

        return redirect('Empleado')->with('Mensaje','Empleado Agregado con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function show(empleado $empleado)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //


        $user = User::all();
        $empleado = empleado::findOrFail($id);
        return view('Empleados.Modificar', compact('empleado','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
            $request->validate([
                'nombre' => 'required',
                'apellido' => 'required',
                'user_id' => 'required',
                'Documento' => 'numeric|required',
                'telefono' => 'numeric',
                'direccion' => 'required',
                'estado' => 'required',
            ]);

        $empleados = $request->except(['_token','_method']);
        empleado::where('id','=',$id)->update($empleados);
        return redirect('Empleado')->with('Mensaje','Empleado Actualizado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\empleado  $empleado
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        empleado::destroy($id);
        return redirect('Empleado')->with('Mensaje','Empleado Eliminado con éxito');
    }

    public function exportExcel()
    {
        return Excel::download(new EmpleadoExport, 'empleado.xlsx');
    }
    public function exportPDF(){
        $Empleado = empleado::all();
        $user = User::all();
        $estado = estado::all();
        $pdf = PDF::loadView('Exportar.PDF.Empleado', compact('Empleado','user','estado'));
        return $pdf->download('empleado.pdf');
    }

}
