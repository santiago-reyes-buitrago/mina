<?php

namespace App\Http\Controllers;

use App\Exports\UsuarioExport;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = User::all();
        return view('Usuario.Principal' , compact('user'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::findorfail($id);
        return view('Usuario.Modificar',compact('user'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $Usuarios = request()->except(['_token','_method','Cargo','rol']);
        $Cargo = request('Cargo');
        $remitente = request('email');
        $rol_viejo = request('rol');
        $user = User::find($id);
        $user->update($Usuarios);
        if ($rol_viejo == 'Administrador'){
            $user->removeRole('Administrador');
        }
        elseif ($rol_viejo == 'Empleado'){
            $user->removeRole('Empleado');
        }
        else{
            $user->removeRole('Usuario');
        }
        if($Cargo == 1){

            $user->assignRole('Administrador');

        }
        elseif ($Cargo == 2){

            $user->assignRole('Empleado');
        }
        else {

            $user->assignRole('Usuario');
        }

        Return redirect('Usuario')->with('Mensaje','Usuario actulizado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        User::destroy($id);

        return redirect('Usuario')->with('Mensaje','Usuario eliminada con éxito');
    }

    public function exportExcel()
    {
        return Excel::download(new UsuarioExport, 'usuario.xlsx');
    }

    public function exportPDF(){
        $user = User::all();
        $pdf = PDF::loadView('Exportar.PDF.Usuario', compact('user'));
        return $pdf->download('usuario.pdf');
    }
}
