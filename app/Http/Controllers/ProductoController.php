<?php

namespace App\Http\Controllers;

use App\empleado;
use App\Exports\ProductoExport;
use App\producto;
use App\punto;
use App\tipoproducto;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ProductoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $empleado = empleado::all();
        $punto = punto::all();
        $producto = producto::all();
        $tipos = tipoproducto::all();
        $Esmeraldas = DB::table('productos')->select(DB::raw('count(*) as esmeralda'))->where('tipo','=','1')->get();
        $Oros = DB::table('productos')->select(DB::raw('count(*) as oro'))->where('tipo','=','2')->get();
        $Tantalios = DB::table('productos')->select(DB::raw('count(*) as tantalio'))->where('tipo','=','3')->get();
        return view('Productos.Principal' , compact('producto','punto','empleado','tipos','Esmeraldas','Oros','Tantalios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $empleado = empleado::all();
        $punto = punto::all();
        $tipo = tipoproducto::all();
        return view('Productos.Crear',compact('empleado','punto','tipo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'peso' => 'required|numeric',
            'empleado' => 'required',
            'fecha_extraccion' => 'required|date',
            'puntos' => 'required',
            'tipo' => 'required',
            'cantidad' => 'required|numeric',
            'precio' => 'required|numeric',

        ]);
        $producto=request()->except('_token');

        producto::insert($producto);

        return redirect('Producto')->with('Mensaje','Producto Agregado con éxito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show(producto $producto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $empleado = empleado::all();
        $punto = punto::all();
        $tipo = tipoproducto::all();
        $producto = producto::findOrFail($id);
        return view('Productos.Modificar', compact('empleado','producto','punto','tipo'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $productos = request()->except(['_token','_method']);
        producto::where('id','=',$id)->update($productos);
        return redirect('Producto')->with('Mensaje','Producto actualizado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        producto::destroy($id);
        return redirect('Producto')->with('Mensaje','Producto eliminado con éxito');
    }

    public function exportExcel()
    {
        return Excel::download(new ProductoExport, 'producto.xlsx');
    }

    public function exportPDF(){
        $empleado = empleado::all();
        $punto = punto::all();
        $producto = producto::all();
        $tipos = tipoproducto::all();
        $pdf = PDF::loadView('Exportar.PDF.Producto', compact('empleado','punto','producto','tipos'));
        return $pdf->download('producto.pdf');
    }
}
