<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => ['role:Administrador']], function () {
    Route::resource('Empleado','EmpleadoController');
    Route::get('/Empleado','EmpleadoController@index')->name('Empleado');
    Route::resource('Usuario','UserController');
    Route::get('/Usuario','UserController@index')->name('Usuario');
    Route::get('/Exportar/Empleado/Excel','EmpleadoController@exportExcel');
    Route::get('/Exportar/Empleado/PDF', 'EmpleadoController@exportPDF');
    Route::get('/Exportar/Usuario/Excel', 'UserController@exportExcel');
    Route::get('/Exportar/Usuario/PDF', 'UserController@exportPDF');
});
Route::group(['middleware' => ['role:Empleado|Administrador']], function () {
    Route::resource('Producto','ProductoController');
    Route::get('/Producto','ProductoController@index')->name('Producto');
    Route::get('/Exportar/Producto/Excel','ProductoController@exportExcel');
    Route::get('/Exportar/Producto/PDF','ProductoController@exportPDF');
});
Route::get("/Prueba", function (){
    return view('Inicio');
});


