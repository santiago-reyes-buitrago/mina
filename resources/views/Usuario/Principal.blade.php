@extends('layouts.Plantilla')
@section('Iconos')
    <link href="{{ asset('Iconos/Usuario.ico') }}" rel="shortcut icon">
    <title>Modulo - Usuario</title>
@endsection
@section('content')
    <div>
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Usuarios</h1>
            <a href="#" data-toggle="modal" data-target="#Reporte" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-download fa-sm text-white-50"></i> Generar Reporte</a>
        </div>
        <br>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="myTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Rol</th>
                            <th>Acciones</th>
                        </tr>
                        </thead><tbody>
                        @foreach($user as $usuario)
                            @if($usuario->id == Auth::user()->id)
                            @else
                                <tr>
                                    <td>{{$usuario->id}}</td>
                                    <td>{{$usuario->name}}</td>
                                    <td>{{$usuario->email}}</td>
                                    @if($usuario->hasrole('Administrador'))
                                        <td>Administrador</td>
                                    @elseif($usuario->hasrole('Empleado'))
                                        <td>Empleado</td>
                                    @elseif($usuario->hasrole('Usuario'))
                                        <td>Usuario</td>
                                    @else
                                        <td>N/A</td>
                                    @endif
                                    <td><a href="{{ url('/Usuario/'.$usuario->id.'/edit')}}" class="btn btn-primary Iconos">
                                            <span class="material-icons">edit</span></a>
                                        <form class="Borrar" action="{{ url('/Usuario/'.$usuario->id) }}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger Iconos" onclick="return confirm('¿Desea borrer el registro?')"><span class="material-icons">delete</span></button></form></td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Rol</th>
                            <th>Acciones</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

