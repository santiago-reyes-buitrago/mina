@extends('layouts.Plantilla')
@section('content')
    <form action="{{ url('/Usuario/'.$user->id) }}" method="post">
        {{ csrf_field()  }}
        {{ method_field('PATCH') }}
        @include('Usuario.Formulario', ['Modo' => 'Modificar'])
    </form>
@endsection
