<div class="container">
    <fieldset>
        <legend align="center">
            @if($Modo=='crear')
                Registro de Usuario
            @else
                Actualizar Usuario
            @endif
        </legend>
        <div class="form-group">
            <label for="name">Nombre del usuario</label>
            <input id="name" name="name" class="form-control" type="text" value="{{ isset($user->name)?$user->name:old('name') }}" required readonly>
        </div>
        <div class="form-group">
            <label for="email">Email del usuario</label>
            <input id="email" name="email" class="form-control" type="text" value="{{ isset($user->email)?$user->email:old('email') }}"  required readonly>
        </div>

        <div class="form-group">
            <label for="Cargo">Seleccionar Cargo</label>
            <select name="Cargo" id="Cargo" class="form-control" required>
                <option>Seleccione una opcion</option>
            @if(Auth::user()->hasrole('Administrador'))
                <option value="1">Administrador</option>
                <option value="2">Empleado</option>
                <option value="3">Usuario</option>
            @endif
            </select>
        </div>
        <div class="form-group">
            <input type="hidden" name="rol" value="@if(Auth::user()->hasrole('Administrador'))Administrador
                @elseif(Auth::user()->hasrole('Empleado'))Empleado
                @elseif(Auth::user()->hasrole('Empleado'))Usuario
                @else
            @endif">
        </div>
        <div style="display: inline">
            <button type="submit" class="btn btn-primary Iconos">
                @if($Modo=='crear')
                    <span class="material-icons">done</span>Agregar
                @else
                    <span class="material-icons">edit</span>Modificar
                @endif
            </button>
            <a href="{{ url()->previous() }}" class="btn btn-secondary Iconos"><span class="material-icons">undo
                            </span>Regresar</a>
        </div>
    </fieldset>
</div>
