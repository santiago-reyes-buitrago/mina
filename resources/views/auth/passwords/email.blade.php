@extends('layouts.Externos')
@section('Titulo')
    <title>Olvidar contraseña</title>
    <link href="{{ asset('Iconos/Logo-dark.png') }}" rel="shortcut icon">
@endsection
@section('Cuerpo')
    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block"><img src="{{ asset('Iconos/Logo-dark.png') }}" style="padding-left:150px; padding-top:150px"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-2">Olvido su contraseña?</h1>
                                        <p class="mb-4">Estas cosas pasan. Ingrese su email para que le
                                            enviemos un enlace para restablecer la contraseña!</p>
                                        @if (session('status'))
                                            <div class="alert alert-success" role="alert">
                                                {{ session('status') }}
                                            </div>
                                        @endif
                                    </div>
                                    <form class="user" method="POST" action="{{ route('password.email') }}">
                                        @csrf
                                        <div class="form-group">
                                            <input type="email" class="form-control form-control-user @error('email') is-invalid @enderror"
                                                   id="exampleInputEmail" aria-describedby="emailHelp"
                                                   placeholder="Ingrese su email..."  name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-user btn-block">
                                            Reset Password
                                        </button>
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="{{ route('register') }}">Registrarse!</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href=" {{ url('/') }}">Ya tienes cuenta? Inicia Sesion!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>
@endsection
