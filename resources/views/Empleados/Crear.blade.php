@extends('layouts.Plantilla')
@section('Iconos')
    <link href=" {{ asset('Iconos/Agregar_Empleado.ico') }}" rel="shortcut icon">
@endsection
@section('content')
    <form action="/Empleado" method="post">
        {{ csrf_field()  }}
        @include('Empleados.Formulario', ['Modo' => 'crear']);
    </form>
@endsection
