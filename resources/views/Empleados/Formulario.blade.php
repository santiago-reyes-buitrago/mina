<div class="container">
            <fieldset>
                <!-- Form Name -->
                <legend align="center">
                    @if($Modo=='crear')
                        Registro de Empleado
                    @else
                        Actualizar Empleado
                    @endif
                </legend>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="nombre">Nombre del empleado</label>
                        <input id="nombre" name="nombre" class="form-control" type="text" value="{{ isset($empleado->nombre)?$empleado->nombre:old('nombre') }}" >
                        {!! $errors->first('nombre','<small class="form-text text-muted">:message</small>') !!}
                    </div>
                    <div class="form-group col-md-6">
                        <label for="apellido">Apellido del empleado</label>
                        <input id="apellido" name="apellido" class="form-control" type="text" value="{{ isset($empleado->apellido)?$empleado->apellido:old('apellido') }}"  >
                        {!! $errors->first('apellido','<small class="form-text text-muted">:message</small>') !!}
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="Documento">Numero de identificacion</label>
                        <input id="Documento" name="Documento" class="form-control" type="number" value="{{ isset($empleado->documento)?$empleado->documento:old('documento') }}"  >
                        {!! $errors->first('Documento','<small class="form-text text-muted">:message</small>') !!}
                    </div>
                    <div class="form-group col-md-4">
                        <label for="user_id">Usuario del empleado</label>
                        <select name="user_id" id="user_id" class="form-control" >
                            <option value="">Seleccione una opcion</option>
                            @foreach($user as $usuario)
                                @if($usuario->hasrole('Empleado'))
                                    <option value="{{ $usuario->id }}">{{ $usuario->name }}</option>
                                @elseif($usuario->hasrole('Administrador'))
                                    <option value="{{ $usuario->id }}">{{ $usuario->name }}</option>
                                @else
                                @endif
                            @endforeach
                        </select>
                        {!! $errors->first('user_id','<small class="form-text text-muted">:message</small>') !!}
                    </div>
                    <div class="form-group col-md-4">
                        <label for="direccion">Dirección del empleado</label>
                        <input type="text" id="direccion" class="form-control" name="direccion" value="{{ isset($empleado->direccion)?$empleado->direccion:old('direccion') }}" >
                        {!! $errors->first('direccion','<small class="form-text text-muted">:message</small>') !!}
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="telefono">teléfono del empleado</label>
                        <input type="number" name="telefono" id="telefono" class="form-control" value="{{ isset($empleado->telefono)?$empleado->telefono:old('telefono') }}" >
                        {!! $errors->first('telefono','<small class="form-text text-muted">:message</small>') !!}
                    </div>
                    <div class="form-group col-md-6">
                            <label for="estado">estado del empleado</label>
                            <select name="estado" id="estado" class="form-control">
                                <option value="">Seleccione una opcion</option>
                                <option value="2">Inactivo</option>
                                <option value="1">Activo</option>
                            </select>
                        {!! $errors->first('estado','<small class="form-text text-muted">:message</small>') !!}
                    </div>
                </div>

                <div style="display: inline;">
                    <button type="submit" style="display: inline-flex" class="btn btn-primary">
                        @if($Modo=='crear')
                            <span class="material-icons">done</span>Agregar
                        @else
                            <span class="material-icons">edit</span>Modificar
                        @endif
                    </button>
                    <a href="{{ url()->previous() }}" style="display: inline-flex" class="btn btn-secondary"><span class="material-icons">undo
                            </span>Regresar</a>
                </div>
            </fieldset>
    </div>

