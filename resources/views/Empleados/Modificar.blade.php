@extends('layouts.Plantilla')
@section('content')
    <form action="{{ url('/Empleado/'.$empleado->id) }}" method="post">
        {{ csrf_field()  }}
        {{ method_field('PATCH') }}
        @include('Empleados.Formulario', ['Modo' => 'Modificar']);
    </form>
@endsection
