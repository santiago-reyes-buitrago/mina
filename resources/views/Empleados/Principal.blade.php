@extends('layouts.Plantilla')
@section('Iconos')
    <link href="{{ asset('Iconos/Empleado.ico') }}" rel="shortcut icon">
    <title>Modulo - Empleado</title>
@endsection
@section('content')
    <div>
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Empleados</h1>
            <a href="#" data-toggle="modal" data-target="#Reporte" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-download fa-sm text-white-50"></i> Generar Reporte</a>

        </div>


        <br>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="d-sm-flex align-items-center justify-content-between mb-1">
                    <a href="{{ url('Empleado/create') }}" class="btn btn-primary">Agregar</a>
                    <a href="#" data-toggle="modal" data-target="#Grafica" class="btn btn-primary">grafica</a>
                </div>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="myTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Documento</th>
                            <th>Usuario</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Dirección</th>
                            <th>Teléfono</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead><tbody>
                        @foreach($Empleado as $Empleados)
                            <tr>
                                <td>{{$Empleados->id}}</td>
                                <td>{{$Empleados->documento}}</td>
                                @foreach($user as $usuario)
                                    @if($Empleados->user_id == $usuario->id)
                                        <td>{{$usuario->name}}</td>
                                    @endif
                                @endforeach
                                <td>{{$Empleados->nombre}}</td>
                                <td>{{$Empleados->apellido}}</td>
                                <td>{{$Empleados->direccion}}</td>
                                <td>{{$Empleados->telefono}}</td>
                                @foreach($estado as $estados)
                                    @if($Empleados->estado == $estados->id)
                                        <td>{{$estados->estado}}</td>
                                    @endif
                                @endforeach
                                <td><a href="{{ url('/Empleado/'.$Empleados->id.'/edit')}}" class="btn btn-primary Iconos">
                                        <span class="material-icons">edit</span></a>
                                    <form class="Borrar" action="{{ url('/Empleado/'.$Empleados->id) }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="btn btn-danger Iconos" onclick="return confirm('¿Desea borrer el registro?')"><span class="material-icons">delete</span></button></form></td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Documento</th>
                            <th>Usuario</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Dirección</th>
                            <th>Teléfono</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('Javascript')
    <script>
        var ctx = document.getElementById('myChart');
        var myLineChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Activos', 'Inactivos'],
                datasets: [{
                    label: '# de Empleados',
                    data: [@foreach($Activos as $activo) {{$activo->Activo }} @endforeach,@foreach($Inactivos as $inactivo) {{$inactivo->Inactivo }} @endforeach],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
@endsection

