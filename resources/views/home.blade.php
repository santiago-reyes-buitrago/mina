@extends('layouts.Plantilla')
@section('Iconos')
    <link href="{{ asset('Iconos/Home.ico') }}" rel="shortcut icon">
    <title>Inicio</title>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Inicio') }}</div>
                @if(Auth::user()->hasRole('Administrador'))
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <h2>Eres un administrador</h2>
                            <p>Nombre: {{ Auth::user()->name }}</p>
                        {{ __('Has Iniciado Sesión!') }}
                    </div>
                @elseif(Auth::user()->hasRole('Empleado'))
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <h2>Eres un empleado</h2>
                            <p>Nombre: {{ Auth::user()->name }}</p>
                            {{ __('Has Iniciado Sesión!') }}
                        </div>
                @elseif(Auth::user()->hasrole('Usuario'))
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            <h2>Eres un usuario</h2>
                            <p>Espera a que te asignen un Cargo</p>
                            {{ __('Has iniciado Sesión!') }}
                            @endif

                    </div>
            </div>
        </div>
    </div>
@endsection
