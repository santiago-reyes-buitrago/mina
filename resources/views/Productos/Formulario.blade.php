<div class="container">
        <fieldset>
            <!-- Form Name -->
            <legend align="center">
                @if($Modo=='crear')
                    Registro de Producto
                @else
                    Actualizar Producto
                @endif
            </legend>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="peso">Peso en kg</label>
                    <input id="peso" name="peso" class="form-control" type="number" value="{{ isset($producto->peso)?$producto->peso:old('peso') }}"   required>
                </div>
                <div class="form-group col-md-6">
                    <label for="empleado">Nombre del empleado</label>
                    <select name="empleado" id="empleado" class="form-control" required>
                        <option>Seleccione una opcion</option>
                        @foreach($empleado as $empleados)
                            @if($empleados->user_id == Auth::user()->id)
                                <option value="{{ $empleados->id }}">{{ $empleados->nombre}} {{$empleados->apellido}}</option>
                            @else
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="fecha_extraccion">Fecha de extraccion</label>
                    <input id="fecha_extraccion" name="fecha_extraccion" class="form-control" type="date" value="{{ isset($producto->fecha_extraccion)?$producto->fecha_extraccion:old('fecha_extraccion') }}"  required>
                </div>
                <div class="form-group col-md-6">
                    <label for="puntos">Lugar de extraccion</label>
                    <select name="puntos" id="puntos" class="form-control" required>
                        <option>Seleccione una opcion</option>
                        @foreach($punto as $puntos)
                            <option value="{{$puntos->id}}">{{$puntos->ubicacion}}. Se extrae {{$puntos->mineral}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="tipo">Tipo del producto</label>
                    <select name="tipo" id="tipo" class="form-control" required>
                        <option>Seleccione una opcion</option>
                        @foreach($tipo as $tipos)
                            <option value="{{$tipos->id}}">{{$tipos->tipo}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-6">
                    <label for="cantidad">cantidad del producto</label>
                    <input name="cantidad" id="cantidad" type="number" class="form-control" value="{{ isset($producto->cantidad)?$producto->cantidad:old('precio') }}" required>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="precio">Precio</label>
                    <input type="number" id="precio" class="form-control" name="precio" value="{{ isset($producto->precio)?$producto->precio:old('precio') }}" required>
                </div>
            </div>

            <div style="display: inline;">
                <button type="submit" class="btn btn-primary Iconos">
                    @if($Modo=='crear')
                        <span class="material-icons">done</span>Agregar
                    @else
                        <span class="material-icons">edit</span>Modificar
                    @endif
                </button>
                <a href="{{ url()->previous() }}" class="btn btn-secondary Iconos"><span class="material-icons">undo
                            </span>Regresar</a>
            </div>
        </fieldset>
    </div>
