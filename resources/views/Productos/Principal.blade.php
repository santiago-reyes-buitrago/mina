@extends('layouts.Plantilla')
@section('Iconos')
    <link href="{{ asset('Iconos/Producto.ico') }}" rel="shortcut icon">
    <title>Modulo - Productos</title>
@endsection
@section('content')
    <div>
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Listado de productos</h1>
            <a href="#" data-toggle="modal" data-target="#Reporte" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-download fa-sm text-white-50"></i> Generar Reporte</a>
        </div>

        <br>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="d-sm-flex align-items-center justify-content-between mb-1">
                    <a href="{{ url('Producto/create') }}" class="btn btn-primary">Agregar</a>
                    <a href="#" data-toggle="modal" data-target="#Grafica" class="btn btn-primary">grafica</a>
                </div>

            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="myTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Peso</th>
                            <th>Tipo</th>
                            <th>Empleado</th>
                            <th>Fecha de extraccion</th>
                            <th>Puntos de extracción</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th>Acciones</th>
                        </tr>
                        </thead><tbody>
                        @foreach($producto as $productos)
                            <tr>
                                <td>{{$productos->id}}</td>
                                <td>{{$productos->peso}}</td>
                                @foreach($tipos as $tipo)
                                    @if($productos->tipo == $tipo->id)
                                        <td>{{$tipo->tipo}}</td>
                                    @endif
                                @endforeach
                                @foreach($empleado as $empleados)
                                    @if($productos->empleado == $empleados->id)
                                        <td>{{$empleados->nombre}} {{$empleados->apellido}}</td>
                                    @endif
                                @endforeach
                                <td>{{$productos->fecha_extraccion}}</td>
                                @foreach($punto as $puntos)
                                    @if($productos->puntos == $puntos->id)
                                        <td>{{$puntos->ubicacion}}</td>
                                    @endif
                                @endforeach
                                <td>{{$productos->precio}}</td>
                                <td>{{$productos->cantidad}}</td>
                                <td><a href="{{ url('/Producto/'.$productos->id.'/edit')}}" class="btn btn-primary Iconos">
                                        <span class="material-icons">edit</span></a>
                                    <form class="Borrar" action="{{ url('/Producto/'.$productos->id) }}" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="btn btn-danger Iconos" onclick="return confirm('¿Desea borrer el registro?')"><span class="material-icons">delete</span></button></form></td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Peso</th>
                            <th>Tipo</th>
                            <th>Empleado</th>
                            <th>Fecha de extraccion</th>
                            <th>Puntos de extracción</th>
                            <th>Precio</th>
                            <th>Cantidad</th>
                            <th>Acciones</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('Javascript')
    <script>
        var ctx = document.getElementById('myChart');
        var myLineChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Esmeralda', 'Oro','Tantalio'],
                datasets: [{
                    label: '# de Gemas',
                    data: [@foreach($Esmeraldas as $esmeralda) {{$esmeralda->esmeralda }} @endforeach,@foreach($Oros as $oro) {{$oro->oro }} @endforeach,@foreach($Tantalios as $tantalio){{ $tantalio->tantalio }}@endforeach],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
@endsection
