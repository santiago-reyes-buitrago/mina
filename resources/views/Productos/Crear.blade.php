@extends('layouts.Plantilla')
@section('content')
    <form action="/Producto" method="post">
        {{ csrf_field()  }}
        @include('Productos.Formulario', ['Modo' => 'crear']);
    </form>
@endsection
