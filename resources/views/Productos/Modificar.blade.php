@extends('layouts.Plantilla')
@section('content')
    <form action="{{url('/Producto/'.$producto->id) }}" method="post">
        {{ csrf_field()  }}
        {{ method_field('PATCH') }}
        @include('Productos.Formulario', ['Modo' => 'Modificar']);
    </form>
@endsection
