<table>
    <thead>
    <tr>
        <th>#</th>
        <th>Documento</th>
        <th>Usuario</th>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Dirección</th>
        <th>Teléfono</th>
        <th>Estado</th>
    </tr>
    </thead>
    <tbody>
    @foreach($Empleado as $Empleados)
        <tr>
            <td>{{$Empleados->id}}</td>
            <td>{{$Empleados->documento}}</td>
            @foreach($user as $usuario)
                @if($Empleados->user_id == $usuario->id)
                    <td>{{$usuario->name}}</td>
                @endif
            @endforeach
            <td>{{$Empleados->nombre}}</td>
            <td>{{$Empleados->apellido}}</td>
            <td>{{$Empleados->direccion}}</td>
            <td>{{$Empleados->telefono}}</td>
            @foreach($estado as $estados)
                @if($Empleados->estado == $estados->id)
                    <td>{{$estados->estado}}</td>
                @endif
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>
