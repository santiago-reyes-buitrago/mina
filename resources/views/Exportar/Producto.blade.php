<table>
    <thead>
    <tr>
        <th>#</th>
        <th>Peso</th>
        <th>Tipo</th>
        <th>Empleado</th>
        <th>Fecha de extraccion</th>
        <th>Puntos de extracción</th>
        <th>Precio</th>
        <th>Cantidad</th>
    </tr>
    </thead>
    <tbody>
    @foreach($producto as $productos)
        <tr>
            <td>{{$productos->id}}</td>
            <td>{{$productos->peso}}</td>
            @foreach($tipos as $tipo)
                @if($productos->tipo == $tipo->id)
                    <td>{{$tipo->tipo}}</td>
                @endif
            @endforeach
            @foreach($empleado as $empleados)
                @if($productos->empleado == $empleados->id)
                    <td>{{$empleados->nombre}} {{$empleados->apellido}}</td>
                @endif
            @endforeach
            <td>{{$productos->fecha_extraccion}}</td>
            @foreach($punto as $puntos)
                @if($productos->puntos == $puntos->id)
                    <td>{{$puntos->ubicacion}}</td>
                @endif
            @endforeach
            <td>{{$productos->precio}}</td>
            <td>{{$productos->cantidad}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
